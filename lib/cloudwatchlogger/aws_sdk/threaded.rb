require 'aws-sdk-cloudwatchlogs'
require 'thread'

module CloudWatchLogger
  class AWS_SDK
    class DeliveryThread < Thread
      def initialize(connection, log_group_name, log_stream_name)
        @queue = Queue.new
        @exiting = false
        @log_stream_name = log_stream_name
        @log_group_name = log_group_name

        super do
          loop do
            @client ||= connection.client

            message_object = @queue.pop
            break if message_object == :__delivery_thread_exit_signal__

            begin
              event = {
                log_group_name: @log_group_name,
                log_stream_name: @log_stream_name,
                log_events: [{
                  timestamp: message_object[:epoch_time],
                  message:   message_object[:message]
                }]
              }
              event[:sequence_token] = @sequence_token if @sequence_token
              response = @client.put_log_events(event)
              unless response.rejected_log_events_info.nil?
                raise CloudWatchLogger::LogEventRejected
              end
              @sequence_token = response.next_sequence_token
            rescue Aws::CloudWatchLogs::Errors::InvalidSequenceTokenException => err
              @sequence_token = err.message.split(' ').last
              retry
            end
          end
        end

        at_exit do
          exit!
          join
        end
      end

      # Signals the queue that we're exiting
      def exit!
        @exiting = true
        @queue.push :__delivery_thread_exit_signal__
      end

      # Pushes a message onto the internal queue
      def deliver(message)
        @queue.push(format(message))
      end

      def format(message)
        datetime = Time.parse(message.match(/(?<=\[)(.*)(?=\])/).to_s.split(" ").first) rescue Time.now
        {
          message:    message,
          epoch_time: epoch_from(datetime)
        }
      end

      def epoch_from(datetime)
        (datetime.utc.to_f.round(3) * 1000).to_i
      end
    end
  end
end
