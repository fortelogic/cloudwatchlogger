require File.join(File.dirname(__FILE__), 'aws_sdk', 'threaded')

module CloudWatchLogger
  class AWS_SDK
    attr_reader :input_uri, :deliverer

    def initialize(credentials, log_group_name, log_stream_name, opts = {})
      setup_prerequisites(credentials, log_group_name, log_stream_name, opts)
      start_thread
    end

    def write(message)
      start_thread unless @upload_thread.alive?
      @upload_thread.deliver(message)
      # Race condition? Sometimes we need to rescue this and start a new thread
    rescue NoMethodError
      @upload_thread.kill # Try not to leak threads, should already be dead anyway
      start_thread
      retry
    end

    def close
      nil
    end

    def client
      return @client if @client
      connect!
      @client
    end

    # def formatter
    #   proc do |severity, datetime, progname, msg|
    #     {
    #       message: "#{datetime}, #{severity}: #{msg} from #{progname} \n",
    #       epoch_time: epoch_from(datetime)
    #     }
    #   end
    # end

    private

    def setup_prerequisites(credentials, log_group_name, log_stream_name, opts)
      raise ArgumentError, "aws_client_key is required in credentials" unless credentials[:access_key_id]
      raise ArgumentError, "secret_access_key is required in credentials" unless credentials[:secret_access_key]
      @credentials = credentials
      raise ArgumentError, "Log group name is mandatory" unless log_group_name
      @log_group_name = log_group_name 
      raise ArgumentError, "Log stream name is mandatory" unless log_stream_name
      @log_stream_name = log_stream_name
      @options = opts
    end

    def connect!
      @client = Aws::CloudWatchLogs::Client.new(
        region: @options[:region] || 'us-east-1',
        access_key_id: @credentials[:access_key_id],
        secret_access_key: @credentials[:secret_access_key],
        http_open_timeout: @options[:open_timeout],
        http_read_timeout: @options[:read_timeout]
      )
      begin
        @client.create_log_stream(
          log_group_name: @log_group_name,
          log_stream_name: @log_stream_name
        )
      rescue Aws::CloudWatchLogs::Errors::ResourceNotFoundException
        @client.create_log_group(
          log_group_name: @log_group_name
        )
        retry
      rescue Aws::CloudWatchLogs::Errors::ResourceAlreadyExistsException
      end
    end

    def start_thread
      @upload_thread = CloudWatchLogger::AWS_SDK::DeliveryThread.new(self, @log_group_name, @log_stream_name)
    end

    # def epoch_from(datetime)
    #   (datetime.utc.to_f.round(3) * 1000).to_i
    # end
  end
end
