require File.join(File.dirname(__FILE__), 'cloudwatchlogger', 'aws_sdk')

require 'logger'

module CloudWatchLogger
  class LogGroupNameRequired < ArgumentError; end
  class LogEventRejected < ArgumentError; end

  def self.new(credentials, log_group_name, log_stream_name = nil, opts = {})
    client = CloudWatchLogger::AWS_SDK.new(credentials, log_group_name, log_stream_name, opts)
    logger = ActiveSupport::Logger.new(client)
    # request id tag is not available in own formatter even initalized with ActiveSupport::TaggedLogging-since it is a different object
    logger.formatter = Rails.logger.formatter
    logger.level = Rails.logger.level

    # if client.respond_to?(:formatter)
    #   logger.formatter = client.formatter
    # elsif client.respond_to?(:datetime_format)
    #   logger.datetime_format = client.datetime_format
    # end
    logger
  end
end
