Gem::Specification.new do |s|
  s.name        = 'cloudwatchlogger'
  s.version     = '1.0.2'
  s.date        = '2019-11-22'
  s.summary     = "Publish log events to AWS Cloudwatch"
  s.description = ""
  s.authors     = [""]
  s.email       = ''
  s.files       = ["lib/cloudwatchlogger.rb"]
  s.homepage    = 'https://rubygems.org/gems/cloudwatchlogger'
  s.add_runtime_dependency 'aws-sdk-cloudwatchlogs'
end
